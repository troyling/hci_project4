import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import models.Album;

import org.xml.sax.SAXException;

public class Main {
	public static void main(String args[]) {
		AlbumImporter importer = new AlbumImporter();
		try {
			List<Album> albums = importer.readFile("./project4.xml");
			for (Album a : albums) {
				System.out.println("Title: " + a.getTitle() + " Artist: " + a.getArtist() + " Genre: " + a.getGenre());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}

	}
}
