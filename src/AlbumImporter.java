import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import models.Album;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class AlbumImporter {

	public AlbumImporter() {

	}

	public List<Album> readFile(String path)
			throws ParserConfigurationException, SAXException, IOException {
		List<Album> albums = new ArrayList<Album>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(ClassLoader
				.getSystemResourceAsStream(path));

		NodeList nodes = doc.getDocumentElement().getChildNodes();

		for (int i = 0; i < nodes.getLength(); i++) {
			String genre, artist, title;
			genre = artist = title = "";

			Node node = nodes.item(i);
			if (node instanceof Element) {
				// parsing
				NodeList childNodes = node.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node child = childNodes.item(j);
					if (child instanceof Element) {
						String content = child.getLastChild().getTextContent()
								.trim();
						switch (child.getNodeName()) {
						case "genre":
							genre = content;
						case "artist":
							artist = content;
						case "title":
							title = content;
						}
					}
				}

			}

			if (!title.isEmpty() || !artist.isEmpty() || !genre.isEmpty()) {
				Album album = new Album(title, artist, genre);
				albums.add(album);
			}
		}
		return albums;
	}
}
